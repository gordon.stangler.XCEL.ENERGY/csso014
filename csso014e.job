#!/usr/bin/ksh -x
############################################################################
#  Script Name:       csso014e.job                                         # 
#                                                                          #
#  Purpose:           Execute:  PUBSO014 - OMS/OCS daily extract           #
#                                                                          #
#  Description:       Extract CRS meter/premise data and FTP to OMS        #
#                                                                          #
#  Potential Changes:                                                      #
#                                                                          #
#  Date Written:      Mar 2008                                             #
#                                                                          #
#  Written by:        Betsy Zelinger                                       #
############################################################################
#                                                                          #
#  Command Line                                                            #
#  Arguments:          Variable      Description                           #
#                      --------      ----------------------------------    #
#                       $0           Configuration File                    #
#                                                                          #
############################################################################
#                                                                          #
#  Revision History:                                                       #
#                                                                          #
#  Revision Date     Revised By     Description                            #
#  -------------     ----------     ----------------------------------     #
#  2018-01-26    Clare Hoff-Podvin  Change to NMS 1.12 server and dir path #
#  2012-04-06    Clare Hoff-Podvin  SC-2638775 change to NMS server & path #
#  2008-12-08    G. Stuvel          Changed to secure ftp transfer         #
#  2020-08-04    G. Stuvel          Changed to use register rules from     # 
#                                   $PIXCONF/csso014.register.rules file to# 
#                                   determine usage.                       #    
############################################################################


############################################################################
####    step10                                                          ####
step step10 csso014e - Set environment password and MAILTO value
############################################################################
execute rm -f $PIXDATA/csso014e.dummy

execute get_passwd $PIXENV

if [ $PIXENV = "prod" ] || [ $PIXENV = "tcrs_shd" ]; then
   MAILTO='dlCRSServiceOrders@xcelenergy.com'
else
   MAILTO='greg.stuvel@xcelenergy.com'
fi


############################################################################
##      step20                                                          ####
step step20 csso014e - get_passwd for OMS/FTP access 
############################################################################
if [ $PIXENV = "prod" ] || [ $PIXENV = "tcrs_shd" ]; then
   execute get_passwd nmspn
else
   execute get_passwd nmstn
fi


############################################################################
##      step30                                                          ####
step step30 csso014e - Execute PUBSO014 to extract OMS data
############################################################################
dd register_rules $PIXCONF/csso014.register.rules   #New register rules table to determine usage
dd debug_premises $PIXDATA/csso014e.new.debug          #Optional list of premise numbers to extract/debug

execute PUBSO014 $PIXBASE/conf/csso014e.cfg

############################################################################
##      step40                                                          ####
step step40 csso014e - Build FTP parameters to send file to OMS server
############################################################################
LOCALFILE=$PIXDATA/OMS_elec_output5

if [ ! -s $LOCALFILE ]; then
   execute ls -l $LOCALFILE  >> $PIXDATA/csso014e.dummy
   execute mailx -s "($PIXENV) csso014e.job output file is empty / NOT FTP'd to OMS" $MAILTO < $PIXDATA/csso014e.dummy
   abend 1
fi

#step 50
#copies BOVINA data to OMS_elec_output3, which is in the south
grep T8 $LOCALFILE | grep BOVINA >> $PIXDATA/OMS_elec_output3
#deletes BOVINA data from $LOCALFILE
sed '/T8T8 BOVINA/d' $LOCALFILE

#step 60
#we then let csso014g finish the FTP transfer for all files
exit 0
