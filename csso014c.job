#!/usr/bin/ksh -x
############################################################################
#  Script Name:       csso014c.job                                         # 
#                                                                          #
#  Purpose:           Execute:  PUBSO014 - OMS/OCS daily extract           #
#                                                                          #
#  Description:       Extract CRS meter/premise data and FTP to OMS        #
#                                                                          #
#  Potential Changes:                                                      #
#                                                                          #
#  Date Written:      Mar 2008                                             #
#                                                                          #
#  Written by:        Betsy Zelinger                                       #
############################################################################
#                                                                          #
#  Command Line                                                            #
#  Arguments:          Variable      Description                           #
#                      --------      ----------------------------------    #
#                       $0           Configuration File                    #
#                                                                          #
############################################################################
#                                                                          #
#  Revision History:                                                       #
#                                                                          #
#  Revision Date     Revised By     Description                            #
#  -------------     ----------     ----------------------------------     #
#  2018-01-26    Clare Hoff-Podvin  Change to NMS 1.12 server and dir path #
#  2012-04-06    Clare Hoff-Podvin  SC-2638775 change to NMS server & path #
#  2008-12-08    G. Stuvel          Changed to secure ftp transfer         #
#  2020-08-04    G. Stuvel          Changed to use register rules from     # 
#                                   $PIXCONF/csso014.register.rules file to# 
#                                   determine usage.                       #    
#  2023-03-24    G. Stangler        Updated c and e job to move BOVINA TX  #
#                                   from the north file to the south file  #
#                                   then ftp them both in e job            #
############################################################################


############################################################################
####    step10                                                          ####
step step10 csso014c - Set environment password and MAILTO value
############################################################################
execute rm -f $PIXDATA/csso014c.dummy

execute get_passwd $PIXENV

if [ $PIXENV = "prod" ] || [ $PIXENV = "tcrs_shd" ]; then
   MAILTO='dlCRSServiceOrders@xcelenergy.com'
else
   MAILTO='greg.stuvel@xcelenergy.com'
fi


############################################################################
##      step20                                                          ####
step step20 csso014c - get_passwd for OMS/FTP access 
############################################################################
if [ $PIXENV = "prod" ] || [ $PIXENV = "tcrs_shd" ]; then
   execute get_passwd nmsps
else
   execute get_passwd nmsts
fi


############################################################################
##      step30                                                          ####
step step30 csso014c - Execute PUBSO014 to extract OMS data
############################################################################
dd register_rules $PIXCONF/csso014.register.rules   #New register rules table to determine usage
dd debug_premises $PIXDATA/csso014c.new.debug          #Optional list of premise numbers to extract/debug

execute PUBSO014 $PIXBASE/conf/csso014c.cfg
 
############################################################################
##      step40                                                          ####
step step40 csso014c - Build FTP parameters to send file to OMS server
############################################################################
LOCALFILE=$PIXDATA/OMS_elec_output3

if [ ! -s $LOCALFILE ]; then
   execute ls -l $LOCALFILE  >> $PIXDATA/csso014c.dummy
   execute mailx -s "($PIXENV) csso014c.job output file is empty / NOT FTP'd to OMS" $MAILTO < $PIXDATA/csso014c.dummy
   abend 1
fi

#step 50
#we then let csso014f finish the FTP transfer for OMS_elec_output3
exit 0
