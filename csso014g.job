#!/usr/bin/ksh -x
############################################################################
#  Script Name:       csso014g.job                                         # 
#                                                                          #
#  Purpose:           Move OMS/OCS daily extract to remote FTP server      #
#                                                                          #
#  Description:       Extract CRS meter/premise data and FTP to OMS        #
#                                                                          #
#  Potential Changes:                                                      #
#                                                                          #
#  Date Written:      Mar 2023                                             #
#                                                                          #
#  Written by:        Gordon Stangler                                      #
############################################################################
#                                                                          #
#  Command Line                                                            #
#  Arguments:          Variable      Description                           #
#                      --------      ----------------------------------    #
#                                                                          #
############################################################################
#                                                                          #
#  Revision History:                                                       #
#                                                                          #
#  Revision Date     Revised By     Description                            #
#  -------------     ----------     ----------------------------------     #
############################################################################


############################################################################
####    step10                                                          ####
step step10 csso014e - Set environment password and MAILTO value
############################################################################
#execute rm -f $PIXDATA/csso014g.dummy

execute get_passwd $PIXENV
 
if [ $PIXENV = "prod" ] || [ $PIXENV = "tcrs_shd" ]; then
   MAILTO='dlCRSServiceOrders@xcelenergy.com'
else
   MAILTO='gordon.c.stangler@xcelenergy.com'
fi

############################################################################
##      step20                                                          ####
step step20 csso014e - get_passwd for OMS/FTP access 
############################################################################
if [ $PIXENV = "prod" ] || [ $PIXENV = "tcrs_shd" ]; then
   execute get_passwd nmspn
else
   execute get_passwd nmstn
fi

############################################################################
##      step30                                                          ####
step step30 csso014e - Build FTP parameters to send file to OMS server
############################################################################
set -A LOCALFILEArray OMS_elec_output1 OMS_elec_output2 OMS_elec_output3 OMS_elec_output5 OMS_elec_output6
set -A REMOTEFILEArray /u12/nms/ps/cust_data/crsomsextf1.`date +%Y-%m-%d`.txt /u12/nms/ps/cust_data/crsomsextf2.`date +%Y-%m-%d`.txt /u12/nms/ps/cust_data/crsomsextf3.`date +%Y-%m-%d`.txt /u12/nms/pn/cust_data/crsomsnthf1.`date +%Y-%m-%d`.txt /u12/nms/pn/cust_data/crsomsnthf2.`date +%Y-%m-%d`.txt

for i in ${!LOCALFILEArray[*]}; do
	LOCALFILE=${LOCALFILEArray[$i]}
	REMOTEFILE=${REMOTEFILEArray[$i]}
	#step step50 csso014e - FTP OMS_extract_output file to OMS server
	if [ $PIXENV = "prod" ] || [ $PIXENV = "tcrs_shd" ]; then
		execute putsftp $LOCALFILE $REMOTEFILE
	else
		#Jobs a,b,c all write to <blah>/ps/<blah>
		if [[ $REMOTEFILE =~ "ps" ]]; then
			REMOTEFILE=${REMOTEFILE/ps/ts}
		fi
		#Jobs e,f all write to <blah>/pn/<blah>
		if [[ $REMOTEFILE =~ "pn" ]]; then
			REMOTEFILE=${REMOTEFILE/pn/tn}
		fi
		execute putsftp $LOCALFILE $REMOTEFILE
	fi

	if [ $? -ne 0 ]; then
		echo "  putsftp $LOCALFILE $REMOTEFILE "
		echo "ERROR! putsftp of CRS/OMS $LOCALFILE to $PIXFTPSITE FAILED"
		echo "ERROR! putsftp of CRS/OMS $LOCALFILE to $PIXFTPSITE FAILED" >> $PIXDATA/csso014g.dummy
	fi
done

############################################################################
##      step40                                                          ####
step step60 csso014e - Check for FTP errors
############################################################################
if [ -s $PIXDATA/csso014g.dummy ]; then
   execute mailx -s "($PIXENV) csso014g.job (CRS-OMS ftp) FAILED" $MAILTO < $PIXDATA/csso014g.dummy
   abend 1
fi

############################################################################
##      step70                                                          ####
step step70 csso014e - FTP trigger file to OMS directory 
############################################################################
set -A touchArray a b c e f
set -A touchRemoteArray REMOTEFILE=/u12/nms/ps/cust_data/CRS_CUSTOMER_DATAF1.TRIGGER /u12/nms/ps/cust_data/CRS_CUSTOMER_DATAF2.TRIGGER /u12/nms/ps/cust_data/CRS_CUSTOMER_DATAF3.TRIGGER /u12/nms/pn/cust_data/CRS_NTHCUSTOMER_DATAF1.TRIGGER REMOTEFILE=/u12/nms/pn/cust_data/CRS_NTHCUSTOMER_DATAF2.TRIGGER

for i in ${!touchArray[*]}; do
	locl="$PIXDATA/csso014"${touchArray[$i]}".trigger"
	execute touch $locl
	#execute touch $PIXDATA/csso014e.trigger
	LOCALFILE=${touchArray[$i]}
	REMOTEFILE=${touchRemoteArray[$i]}

	if [ $PIXENV = "prod" ] || [ $PIXENV = "tcrs_shd" ]; then
		execute putsftp $LOCALFILE $REMOTEFILE
	else
		#Jobs a,b,c all write to <blah>/ps/<blah>
		if [[ $REMOTEFILE =~ "ps" ]]; then
			REMOTEFILE=${REMOTEFILE/ps/ts}
		fi
		#Jobs e,f all write to <blah>/pn/<blah>
		if [[ $REMOTEFILE =~ "pn" ]]; then
			REMOTEFILE=${REMOTEFILE/pn/tn}
		fi
		execute putsftp $LOCALFILE $REMOTEFILE
	fi
done

if [ $? -ne 0 ]; then
      echo "putsftp $LOCALFILE $REMOTEFILE"
      echo "ERROR! putsftp of CRS/OMS $REMOTEFILE to $PIXFTPSITE FAILED"
      echo "ERROR! putsftp of CRS/OMS $REMOTEFILE to $PIXFTPSITE FAILED" >> $PIXDATA/csso014g.dummy
fi

############################################################################
##      step80                                                          ####
step step80 csso014e - Check for FTP trigger file errors
############################################################################
if [ -s $PIXDATA/csso014g.dummy ]; then
   execute mailx -s "($PIXENV) csso014e.job (CRS-OMS ftp) FAILED!!" $MAILTO < $PIXDATA/csso014g.dummy
   abend 1 
fi

############################################################################
##      step90                                                          ####
step step90 csso014e - Cleanup
############################################################################
execute rm -f $PIXDATA/csso014g.trigger
exit 0
